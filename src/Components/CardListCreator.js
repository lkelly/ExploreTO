import React from 'react';
// import './ComponentCSS/Card.css';
import AppCard from './Card';


import {
    Card,
    CardTitle,
    CardMenu,
    IconButton
} from 'react-mdl';

var AppDateCard = React.createClass({
    propTypes: {
        date: React.PropTypes.string.isRequired
    },
    render: function() {
        return <Card shadow={0} className="date-card" style={{
            margin: '0 auto'
        }}>
            <CardTitle>{this.props.date}</CardTitle>
            <CardMenu style={{color: 'grey'}}>
                <IconButton name="list" />
            </CardMenu>
        </Card>
    }
});



var CardListCreator = React.createClass({
  propTypes: {
  eventArray:  React.PropTypes.arrayOf(React.PropTypes.object),

},
  render: function(){ return <div> <AppDateCard date="Today" />
    {this.props.eventArray.map(function(object, i){
        console.log(object.activityName);
        return <AppCard activityName={object.activityName} logoURL={object.logoURL} activityDescription={object.activityDescription} key={i} />;
    })}
  </div>
  }
});






export default CardListCreator;
