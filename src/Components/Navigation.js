import React from 'react';
import './ComponentCSS/Navigation.css';
import { Layout, Header, Navigation, Drawer, Content } from 'react-mdl';

var AppNavigation = React.createClass({
  render: function(){ return <div className="nav-container" style={{ position: 'relative'}}>
    <Layout className="layout-object" style={{background: 'url(http://exploreapp.ca/img/toronto_background.jpg) center / cover'}}>
        <Header transparent title="Explore.TO" style={{color: 'white'}}>
            <Navigation>
                <a href="">Signup</a>
                <a href="">Login</a>
            </Navigation>
        </Header>
        <Drawer title="Explore.TO" className="nav-drawer">
            <Navigation>
                <a href="">Signup</a>
                <a href="">Login</a>

            </Navigation>
        </Drawer>
        <Content >
        <h1 className="intro">Life's too short to be bored</h1>
        <h2 className="tagline">Be the first to discover the best Toronto has to offer</h2>
      </Content>

    </Layout>
</div>
  }
});

export default AppNavigation;
