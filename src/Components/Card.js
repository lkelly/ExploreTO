import React from 'react';
import './ComponentCSS/Card.css';
import { Button, Card, CardText, CardTitle, CardMenu, CardActions, IconButton } from 'react-mdl';


var AppCard = React.createClass({
  propTypes: {
  activityName:  React.PropTypes.string.isRequired,
  logoURL:  React.PropTypes.string.isRequired,
  activityDescription: React.PropTypes.string.isRequired,
  activityURL: React.PropTypes.string,
  activityDate: React.PropTypes.string.isRequired,
},
  render: function(){ return <Card  shadow={0} className="app-card" style={{ margin: '10px auto'}}>
      <CardTitle className="app-card-title">
        <div className="card-pic">
        <img alt="Event logo" src={this.props.logoURL}></img>
      </div>
        {this.props.activityName}
      </CardTitle>

      <CardText>
          {this.props.activityDescription}
      </CardText>
      <CardActions border>
          <Button colored>Check it out</Button>
      </CardActions>
      <CardMenu className="card-icons" style={{color: 'grey'}}>
          <IconButton name="favorite_border" />
          {/* <IconButton name="share" />
          <IconButton name="more_vert" /> */}
      </CardMenu>
  </Card>
  }
});




export default AppCard;
