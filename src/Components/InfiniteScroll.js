import React from 'react';

import AppCard from './Card';


import {
    Card,
    CardTitle,
    CardMenu,
    IconButton
} from 'react-mdl';

var Waypoint = require('react-waypoint');



var AppDateCard = React.createClass({
    propTypes: {
        date: React.PropTypes.string.isRequired
    },
    render: function() {
        return <Card shadow={0} className="date-card" style={{
            margin: '0 auto'
        }}>
            <CardTitle>{this.props.date}</CardTitle>
            <CardMenu style={{color: 'grey'}}>
                <IconButton name="list" />
            </CardMenu>
        </Card>
    }
});







/**
 * Randomly return either a cat or machine image url
 * @return {string}
 */
var currentIndex = 0;



// function fetchDemo() {
//   var myUrl = 'https://app.ticketmaster.com/discovery/v1/events.json?apikey=34RENlQMEOAy9ABHWTzAKku5BPIQ3x9v';
//         return fetch(myUrl).then(function(response) {
//             return response.json();
//         }).then(function(json) {
//             return json._embedded.events;
//         });
//     }
//
// fetchDemo().then(function(result) {
// console.log(result[0].name);
// });




var generateItem = function() {


    var newItem = {};
    newItem.activityName = "Generated card number " + currentIndex;
    newItem.logoURL = "http://toronto.bluejays.mlb.com/mlb/images/team_logos/social_media/og_1200x630_image/tor_1200x630.jpg";
    newItem.activityDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis pellentesque lacus eleifend lacinia..."
    newItem.activityDate = "Tomorrow";

    currentIndex++;
    return newItem;


}

var InfiniteScroll = React.createClass({
  propTypes: {
  eventArray:  React.PropTypes.arrayOf(React.PropTypes.object),

},
  _loadMoreItems: function() {
    var itemsToAdd = 3;
    var secondsToWait = 2;
    this.setState({ isLoading: true });
    // fake an async. ajax call with setTimeout
    window.setTimeout(function() {
      // add data
      var currentItems = this.state.items;
      for (var i = 0; i < itemsToAdd; i++) {
        currentItems.push(generateItem());
      }
      this.setState({
        items: currentItems,
        isLoading: false,
      });
    }.bind(this), secondsToWait * 500);
  },



  /**
   * @return {Object}
   */
  getInitialState: function() {
    return {
      items: this.props.eventArray,
      isLoading: false,
    };
  },

  /**
   * @return {Object}
   */
  _renderItems: function() {
    var cur_date;
    return this.state.items.map(function(object, i){
        //console.log(object.activityName);
        if(object.activityDate !== cur_date){
          cur_date = object.activityDate;

          return (
            <div key={i}>
            <AppDateCard date={cur_date} />
            <AppCard
              activityName={object.activityName}
              logoURL={object.logoURL}
              activityDescription={object.activityDescription}
              activityDate={object.activityDate}
               />
            </div>);

        }

        return (
          <AppCard
            activityName={object.activityName}
            logoURL={object.logoURL}
            activityDescription={object.activityDescription}
            activityDate={object.activityDate}
            key={i} />);
    });

  },


  _renderWaypoint: function() {
    if (!this.state.isLoading) {
      return (
        <Waypoint
          onEnter={this._loadMoreItems}
        />
      );
    }
  },

  /**
   * @return {Object}
   */
  render: function() {
    return (
      <div className="infinite-scroll-example">
        <p style={{textAlign: 'center'}} className="infinite-scroll-example__count">
          Items Loaded: {this.state.items.length}
        </p>
        <div className="infinite-scroll-example__scrollable-parent">
          {this._renderItems()}
          <div className="infinite-scroll-example__waypoint">
            {this._renderWaypoint()}
            Loading more items…
          </div>
        </div>
        <p className="infinite-scroll-example__arrow" />
      </div>
    );
  }
});

export default InfiniteScroll;
