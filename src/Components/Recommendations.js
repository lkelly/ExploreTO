import React from 'react';
import './ComponentCSS/Recommendations.css';
import { Card, CardTitle, CardActions } from 'react-mdl';

var Recommendation = React.createClass({
  propTypes: {
  recommendationName:  React.PropTypes.string.isRequired,
  url:  React.PropTypes.string.isRequired,
},
handleClick() {
    //doSomething
    window.location = '/';
},
  render: function(){
    var imgUrl = this.props.url;
        var cardStyle = {
            backgroundImage: 'url(' + imgUrl + ')',
            display: 'inline-block',
            background: 'url(' + imgUrl + ') center / cover', margin: 'auto',
        }
    return <Card onClick={this.handleClick} className="recommendation" shadow={0} style={cardStyle}>
    <CardTitle expand />
    <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            {this.props.recommendationName}
        </span>
    </CardActions>
</Card>
  }
});

var AppRecommendations = React.createClass({
//   propTypes: {
//   activityName:  React.PropTypes.string.isRequired,
// },
  render: function(){ return <div className="recommendation-container" style={{textAlign: 'center'}}>
      <h3 id="rec-title">Recommendations</h3>
      <Recommendation url="http://media.istockphoto.com/photos/american-soccer-stadium-picture-id465993064?k=6&m=465993064&s=170667a&w=0&h=bJEcMfUvgnbDrpIYuBTjE1RSTi6IHhXQUsWWuJjyjmU=" recommendationName="Sports" />
      <Recommendation url="http://www.thatericalper.com/wp-content/uploads/2014/10/g_live_music.jpeg" recommendationName="Live Music" />
      <Recommendation url="http://www.hellobc.com/getmedia/ec32812f-2ea6-4c34-9e19-a2fca4998c9f/1-2610-Cowichan-Valley.jpg.aspx" recommendationName="Outdoor Activities" />
      <Recommendation url="https://www.lonelyplanet.com/travel-blog/tip-article/wordpress_uploads/2014/12/522311393.jpg" recommendationName="Nightlife" />
      <Recommendation url="http://landmarkhospitality.com/wp-content/uploads/SH_PatioParty015.jpg" recommendationName="Best Patios" />
</div>
  }
});




export default AppRecommendations;
