import React from 'react';
import ReactDOM from 'react-dom';

import AppNavigation from './Components/Navigation';
// import AppCard from './Components/Card';
import AppFooter from './Components/Footer';
import AppRecommendations from './Components/Recommendations';
// import CardListCreator from './Components/CardListCreator';

import InfiniteScroll from './Components/InfiniteScroll';

import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import './index.css';

// import {
//     Card,
//     CardTitle,
//     CardMenu,
//     IconButton
// } from 'react-mdl';


var events = [
    {activityDate:"Today", activityName:"Blue Jays vs Red Sox", logoURL:"http://toronto.bluejays.mlb.com/mlb/images/team_logos/social_media/og_1200x630_image/tor_1200x630.jpg", activityDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis pellentesque lacus eleifend lacinia..."},
    {activityDate:"Today", activityName:"Toronto Wine Fest", logoURL:"http://foodiesonfoot.ca/wp-content/uploads/2015/09/steak-300x300.jpg", activityDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis pellentesque lacus eleifend lacinia..."},
    {activityDate:"Today", activityName:"ROM Presents: Picasso", logoURL:"http://thepeerproject.com/wp-content/uploads/2016/01/ROM_logo.jpg", activityDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis pellentesque lacus eleifend lacinia..."},
    {activityDate:"Today", activityName:"Toronto Raptors vs Boston Celtics", logoURL:"http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/tor.png", activityDescription:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis pellentesque lacus eleifend lacinia..."}


  ];

  // function EventList(props) {
  //   const eventArray = props;
  //   const eventItems = eventArray.map((object, i) =>
  //     <AppCard key={i}
  //     activityName={object.activityName}
  //     logoURL={object.logoURL} />
  //   );
  //   return (
  //     <div>
  //       {eventItems}
  //     </div>
  //   );
  // }

ReactDOM.render(
    <AppNavigation/>, document.getElementById('head'));

ReactDOM.render(
    <div>
    <AppRecommendations />
    <div style={{ height: '50vw', minHeight: '500px', width: '80%', overflow: 'auto', margin: '50px auto', minWidth: '280px' }}>
    <InfiniteScroll eventArray={events}/>
  </div>
    {/* <CardListCreator eventArray={events} /> */}



</div>, document.getElementById('content'));

ReactDOM.render(
    <AppFooter/>, document.getElementById('footer'));
